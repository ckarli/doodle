# coding=utf-8

from django import forms
from django.contrib import admin
from django.db.models import Q
from logomanager.models import Specialday


class SpecialdayForm(forms.ModelForm):
    class Meta:
        model = Specialday
        exclude = ()

    def clean(self):
        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        a=Specialday.objects.filter((Q(start_date__lt = start_date) & Q(end_date__gt = start_date)) |
                                       (Q(start_date__lt=end_date) & Q(end_date__gt=end_date))
                                       )

        if a.count()>0:

            raise forms.ValidationError(("Bu tarih aralığına denk gelen bir özel gün var: %s" % a[0]))




class SpecialdayAdmin(admin.ModelAdmin):
    form = SpecialdayForm
    list_display = ('description', 'start_date', 'end_date', 'logo')



admin.site.register(Specialday, SpecialdayAdmin)