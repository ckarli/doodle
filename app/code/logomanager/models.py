# coding=utf-8

from __future__ import unicode_literals

from django.db import models


class Specialday(models.Model):
    description = models.CharField(blank=False, null=False, max_length=50)
    start_date = models.DateField(verbose_name=u"Gösterim Başlangıç Tarihi", blank=False, unique=True)
    end_date = models.DateField(verbose_name=u"Gösterim Bitiş Tarihi", blank=False, unique=True)
    logo = models.FileField(verbose_name=u"Gösterilecek logo", upload_to='media/')

    def __str__(self):
        return u"%s between %s - %s" % (self.description, self.start_date, self.end_date)

    class Meta:
        managed = True
