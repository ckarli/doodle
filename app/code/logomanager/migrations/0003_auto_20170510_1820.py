# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-10 15:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logomanager', '0002_auto_20170510_1816'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialday',
            name='end_date',
            field=models.DateField(unique=True, verbose_name='G\xf6sterim Biti\u015f Tarihi'),
        ),
        migrations.AlterField(
            model_name='specialday',
            name='start_date',
            field=models.DateField(unique=True, verbose_name='G\xf6sterim Ba\u015flang\u0131\xe7 Tarihi'),
        ),
    ]
