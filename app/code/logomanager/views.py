# coding=utf-8
import os

from PIL import Image
from django.conf import settings
from django.db.models import Q
from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.cache import cache_page
from django.views.generic import ListView

from .models import Specialday


class ShowImage(ListView):

    def get_date(self, date):
        qs=Specialday.objects.filter((Q(start_date__lte = date) & Q(end_date__gte = date)))
        return qs.first()

    #@cache_page(60 * 60 * 12)
    def get(self, request):
        image = self.get_date(timezone.datetime.today())
        if image is None:
            image_url = os.path.join(settings.MEDIA_ROOT, 'media/default.png')
        else:
            #image_url = os.path.join(settings.MEDIA_ROOT, image.logo)
            image_url =  image.logo.path
        try:
            with open(image_url, "rb") as f:
                return HttpResponse(f.read(), content_type="image/jpeg")
        except IOError:

            red = Image.new('RGBA', (1, 1), (255, 0, 0, 0))
            response = HttpResponse(content_type="image/jpeg")
            red.save(response, "JPEG")
            return response
